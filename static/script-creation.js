'use strict';

const description = $('#description')
const departement = $('#departement')
const sous_depart = $('#sousdepartement')
const categorie = $('#categorie')
const div_alerte = $('#alert')

function addData(item) {
    this.append(`<option value="${item.id}">${item.description}</option>`)
}


function GenererListe(url,listeVisee, idlisteSelectionne) {
    const listeselectionne = $("#"+idlisteSelectionne)
    $.get({
        url:'/produit/' + url + "?id=" + listeselectionne.val(),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (donnees, statut, erreur){
            donnees.forEach(addData, listeVisee);
            },
            error: function(jqXHR, statut, erreur){
                 alert(erreur)
             }
        })
}

function ReinitialiserListe(liste) {
    liste.empty()
    liste.append(`<option value=""></option>`)
}

function showAlert(messages, type){
    div_alerte.empty();
    let liste_message = $('<ol></ol>');
    liste_message.addClass("list-group");
    for (const message of messages) {
        let msg = $(`<li>${message}</li>`)
        msg.addClass("list-group-item-success")
        liste_message.append(msg);
    }
    const alerte = $(`<div class="alert alert-${type}" role="alert"></div>`).append(liste_message);
    div_alerte.append(alerte);
}

function ValiderFormulaire(){
    let valide = true
    let messages = []
    const desc = description.val()
    const depart = departement.val()
    const sous_dep = sous_depart.val()
    const cat = categorie.val()
    if (desc === ""){
        valide= false
        messages.push("Description manquante")
        description.parent().css("color", "red")
    }

    if (depart === ""){
        valide= false
        messages.push("Département manquant")
        departement.parent().css("color", "red")
    }
    if (sous_dep === ""){
        valide= false
        messages.push("Sous-département manquant")
        sous_depart.parent().css("color", "red")
    }
    if (cat === ""){
        valide= false
        messages.push("Catégorie manquante")
        categorie.parent().css("color", "red")
    }

    if (!valide){
        showAlert(messages, "warning")
    }
    return valide
}



$(document).ready(function (){
    departement.change(function () {
        ReinitialiserListe(sous_depart)
        ReinitialiserListe(categorie)
        GenererListe( "departement_selectionne",sous_depart, "departement")
    })
    sous_depart.change(function () {
        GenererListe("sous_depart_selectionne",categorie, "sousdepartement")
        ReinitialiserListe(categorie)
    })
    $("#soumettre").click(function (e){
        const produit = { description: description.val(), departement: departement.val(), sousdepartement: sous_depart.val(), categorie: categorie.val() };
        $('form').validate({
            rules:{
                description: "required",
                departement: "required",
                sousdepartement: "required",
                categorie: "required"
            },
            messages: {
                description: "Veuillez entrer une description",
                departement: "Veuillez entrer un département",
                sousdepartement: "Veuillez entrer un sous-département",
                categorie: "Veuillez entrer une catégorie"
            },
            submitHandler: function (form) {
                $.post({
                url:'/creer_produit',
                data: JSON.stringify(produit),
                contentType: "application/json",
                dataType: "json",
                success: function (donnees, statut, erreur){
                    showAlert(["Produit créer avec succès !!!"], "success")
                    setTimeout(function () {
                        div_alerte.hide()
                    },2000)
                },
                error: function(jqXHR, statut, erreur){
                    alert(erreur)
                }
            })
            }

        });
    })
})

function GenererListe(url, listeVisee, idlisteSelectionne) {
	const listeselectionne = $("#" + idlisteSelectionne);
	$.get({
		url: '/produit/' + url + "?id=" + listeselectionne.val(),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(donnees, statut, erreur) {
			donnees.forEach(addData, listeVisee);
		},
		error: function(jqXHR, statut, erreur) {
			alert(erreur);
		}
	});
}
