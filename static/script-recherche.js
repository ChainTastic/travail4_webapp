

function ajouterResultat(resultat) {
    let result = $(`<li>${resultat.nom}</li>`)
    result.addClass('autocomplete-item')
    $('.autocomplete-items').append(result)
}

function AjouterDonnesTableau(donnee) {
    let result = $(`<tr>
<td>${donnee.nom}</td>
<td>${donnee.departement}</td>
<td>${donnee.sousdepartement}</td>
<td>${donnee.categorie}</td>
</tr>`)
    $('#resultats').append(result)
}

function GenererResultats() {
    $.get({
        url:'/produit/recherche?q=' + $('#description').val() ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (donnees, statut, erreur){
            for (const donnee of donnees) {
                ajouterResultat(donnee)
            }
            },
            error: function(jqXHR, statut, erreur){
                 alert(erreur)
             }
        })
}


function GenererTableauResultats() {
        $.get({
        url:'/produit/recherche/resultat?q=' + $('#description').val() ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (donnees, statut, erreur){
            $('#resultats').empty()
            for (const donnee of donnees) {
                AjouterDonnesTableau(donnee)
            }
            },
            error: function(jqXHR, statut, erreur){
                 alert(erreur)
             }
        })
}

$(document).ready(function () {
    $('#description').keyup(function () {
        $('.autocomplete-items').empty()
        if ($('#description').val() !== ''){
            GenererResultats()
        }

    })
    $('.autocomplete-items').on('click', 'li', function () {
        $('#description').val($(this).text())
        $('.autocomplete-items').empty()
    })
    $('form').submit(function (e){
        if ($('#description').val() !== '') {
            GenererTableauResultats()
            $('table').show()
        }
        e.preventDefault()
    })
});