from flask import Flask, render_template, request, jsonify, abort
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost:3306/420-05c_tp4'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class Produits(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60))
    idDepartement_produit = db.Column(db.CHAR(4), db.ForeignKey('departements.id'))
    idSousDepartement_produit = db.Column(db.CHAR(5), db.ForeignKey('sousdepartements.id'))
    idCategorie_produit = db.Column(db.CHAR(6), db.ForeignKey('categories.id'))


class Departements(db.Model):
    id = db.Column(db.CHAR(4), primary_key=True)
    description = db.Column(db.VARCHAR(25))


class Sousdepartements(db.Model):
    id = db.Column(db.CHAR(5), primary_key=True)
    description = db.Column(db.VARCHAR(30))
    idDepartement = db.Column(db.CHAR(4), db.ForeignKey('departements.id'))


class Categories(db.Model):
    id = db.Column(db.CHAR(6), primary_key=True)
    description = db.Column(db.VARCHAR(60))
    idSousDepartement = db.Column(db.CHAR(5), db.ForeignKey('sousdepartements.id'))


def obtenir_liste(id_selectionne, param):
    retour = []
    if param == 'sousdepartements':
        liste_resultat = Sousdepartements.query.filter_by(idDepartement=id_selectionne).all()
    if param == 'categories':
        liste_resultat = Categories.query.filter_by(idSousDepartement=id_selectionne).all()
    for resultat in liste_resultat:
        retour.append({'id': resultat.id, 'description': resultat.description})
    return retour


@app.route('/')
def creation_produit():
    departements = Departements.query.all()
    return render_template('index.html', departements=departements)


@app.route('/creer_produit', methods=['POST'])
def creation_produit_post():
    try:

        produit_json = request.json
        description = produit_json['description']
        departement_id = produit_json['departement']
        sous_departement_id = produit_json['sousdepartement']
        categorie_id = produit_json['categorie']
        produit = Produits(nom=description, idDepartement_produit=departement_id,
                           idSousDepartement_produit=sous_departement_id, idCategorie_produit=categorie_id)
        db.session.add(produit)
        db.session.commit()
        return ('', 204)
    except Exception as e:
        print(str(e))
        abort(500)


@app.route('/produit/departement_selectionne')
def obtenir_sous_depart():
    departement_id = request.args.get('id')
    liste_retour = obtenir_liste(departement_id, 'sousdepartements')
    return jsonify(liste_retour)


@app.route('/produit/sous_depart_selectionne')
def obtenir_categories():
    sousdepart_id = request.args.get('id')
    liste_retour = obtenir_liste(sousdepart_id, 'categories')
    return jsonify(liste_retour)


@app.route('/recherche')
def recherche():
    return render_template('recherche.html')


@app.route('/produit/recherche')
def obtenir_resultat():
    retour = []
    recherche = request.args.get('q')
    liste_resultats = Produits.query.filter(Produits.nom.like('%' + recherche + '%')).limit(5)
    for resultat in liste_resultats:
        retour.append({'id': resultat.id, 'nom': resultat.nom})
    return jsonify(retour)


@app.route('/produit/recherche/resultat')
def obtenir_resultat_tableau():
    retour = []
    recherche = request.args.get('q')
    liste_resultats = Produits.query.filter(Produits.nom.like('%' + recherche + '%'))
    for resultat in liste_resultats:
        retour.append({'id': resultat.id, 'nom': resultat.nom, 'departement': resultat.idDepartement_produit, 'sousdepartement': resultat.idSousDepartement_produit, 'categorie': resultat.idCategorie_produit})
    return jsonify(retour)


if __name__ == '__main__':
    app.run()
